const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

//checking email
router.post("/checkEmail", (req, res)=> {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

//user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//user login
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

//auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was creeated for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))	
})

/*MINIACTIVITY
create request under '/enroll' endpoint
check if user is logged in
create var & store an object w userId & courseId as fields
	userId - decoded token of user
	courseId course id present in request body

user the var as parameter of enroll function

send screenshot of codes in google chat
*/

//using PATCH won't u[date time when user enrolled; it'll only record time when user 1st enrolled. DO NOT USE PATCH

router.post("/enroll", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was created for the user
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		//in the decoded token, the only thing we'll be looking at/storing is the .id
		courseId: req.body.courseId

	}
	userController.enroll(data).then(result => res.send(result))	
})

module.exports = router