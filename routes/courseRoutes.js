const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController")
const userController = require("../controllers/userController.js")


router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})


/*
router.post("/courses", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was creeated for the user
	const adminData = auth.decode(req.headers.authorization)
	userController.addCourse({
		userId: userData.id,
		isAdmin: userData.isAdmin
		})
	.then(resultFromController => res.send(resultFromController))	
})
*/

/*ecommerce websites*/

//create route to retrieve all products/services; no login functions required

router.get("/", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})


// retrieve all active courses
router.get("/active", (req, res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})


//In getting documents, find with params must come in last

//MINIACTIVITY-route that'll retrieve course w/o login from user

// retrieve a course
router.get("/:courseId",  (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params.courseId).then(result => res.send(result))
})


// update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

/*
delete is never a norm in databases

use /archiveCourse and send a PUT request to archive a course by changing the active status
*/
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archivedCourse(req.params, req.body).then(result => res.send(result))
})














module.exports = router