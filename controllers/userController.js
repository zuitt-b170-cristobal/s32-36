//dependencies
const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")//used to encrypt user password

//check if the email exists
/*
	1. check for email in the database
	2. send the result as a response (w error handling)
*/
/*
conventional for devs to use Boolean in sending responses esp w backend app
*/
module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				//return result
				return true
			}else{
				//return res.send("email does not exist")
				return false
			}
		}
	})
}

//USER REGISTRATION
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		//hashSync is a function of bcrypt that encrypts the password
			//10 is the # of times it runs the algorithm to the reqBody.password; max 72 implementations for security measures
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

//USER LOGIN

module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			// compareSync function - used to compare a non-encrypted password to an encrypted password and retuns a Boolean reasponse depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
false - the passwords do not match, thus a toke should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if (result === null) {
			return false
		} else {
			result.password = "*****"; //make password invisible
			return result
		}
	})
}

/*
1. find user/doc in database
2. add courseId to user's enrollment array
3. save the doc in database

*/
module.exports.enroll = async (data, requestBody) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		//adding the courseId to the user's enrollment array
		user.enrollments.push({courseId: data.courseId})
		//saving in database
		return user.save().then((user, err) => {
			if (err){
				return false
			}else{
				return true
			}
		})
	})
/*use another await keyword to update the enrollees array in the course collection
            find the courseId from the requestBody
            push the userId of the enrollee in the enrollees array of the course
            update the document in the database
            
            if both pushing are successful, return true
            if both pushing are not successful, return false*/
    let enrolleesUpdated = await Course.findById(data.courseId).then(user => {
    	user.enrollees.push({userId: data.userId})
    	//saving in database
		return user.save().then((user, err) => {
			if (err){
				return false
			}else{
				return true
			}
		})
	})
    

	if (isUserUpdated, enrolleesUpdated){
		return true
	}
	else{
		return false
	}
}
