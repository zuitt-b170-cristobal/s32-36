//dependencies
const User = require("../models/user.js")
const Course = require("../models/course.js")
//const auth = require("../auth.js")
//const bcrypt = require("bcrypt")//used to encrypt user password


module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}


//MINIACTIVITY-using course id (params) in the URL, retrieve a course using get request

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	})
}


// retrieve all active courses
module.exports.getActive = () => {
	return Course.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}


// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

// archive a course
/*
	1. create a updateCourse object with the content from the reqBody
			reqBody should have the isActive status of the course to be set to false
	2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
			handle the errors that may arise
				error/s - false
				no errors - true
*/
module.exports.archivedCourse = (reqParams, reqBody) => {
		let updatedCourse = {
			isActive: reqBody.isActive
	}

/*
if you don't want to require any request body:
module.exports.archivedCourse = (reqParams) =>{
	let updatedCourse = {
	isActive: false
	}
}
*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}
